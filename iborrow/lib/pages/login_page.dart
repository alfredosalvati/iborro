import 'package:iborrow/authentication/apple_login.dart';
import 'package:iborrow/authentication/google_login.dart';
import 'package:iborrow/common/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:iborrow/config/application.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  @override
  void initState() {
    super.initState();


    Future.delayed(const Duration(milliseconds: 500), () {

      _visible = true;
      setState(() {
        // Here you can write your code for open new view
      });

    });

  }

  bool _visible = false;

  @override
  Widget build(BuildContext context) {

    return
      Scaffold (
        body: Container(
            height: double.infinity,
            width: double.infinity,
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                    padding: EdgeInsets.only(top: 20,),
                    width: double.infinity,
                    height: 150,
                    child: Center(
                      child: Text(
                        'iBorro',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.getFont('Karla',
                          textStyle:
                          TextStyle(color: AppColors.royal,
                              fontSize: 86, fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    )
                ),
                Image.asset('assets/img/background.jpg'),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    AnimatedOpacity(
                        opacity: _visible ? 1.0 : 0.0,
                        duration: Duration(milliseconds: 1500),
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 20, ),
                          child: GoogleLogin(),
                        )
                    ),
                    ApplicationSingleton.appleAvailable && ApplicationSingleton.appleLogin ? AnimatedOpacity(
                      // If the widget is visible, animate to 0.0 (invisible).
                      // If the widget is hidden, animate to 1.0 (fully visible).
                        opacity: _visible ? 1.0 : 0.0,
                        duration: Duration(milliseconds: 1500),
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 20),
                          child: AppleLogin(),
                        )
                    ) : Container()
                  ],
                )

              ],
            )
        ),
      );


  }
}
