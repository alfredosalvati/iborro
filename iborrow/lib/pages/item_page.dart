import 'dart:async';

import 'package:animations/animations.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geolocator/geolocator.dart';
import 'package:iborrow/bloc/cart_bloc.dart';
import 'package:iborrow/common/app_bar.dart';
import 'package:iborrow/common/app_colors.dart';
import 'package:iborrow/config/application.dart';
import 'package:iborrow/dto/jobDto.dart';
import 'package:iborrow/io/firestore.dart';
import 'package:iborrow/widget/cropper.dart';
import 'package:iborrow/widget/objecto.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import 'object_view.dart';



class AvailableObjects extends StatefulWidget {
  final CourierAppBar appBar;

  AvailableObjects({Key key, @required this.appBar}) : super(key: key);

  @override
  _AvailableObjectsState createState() => _AvailableObjectsState();
}

FirestoreUtility firestoreUtility = new FirestoreUtility();

class _AvailableObjectsState extends State<AvailableObjects> {

  @override
  void initState() {
    // loading = true;
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<SessionBloc>(context);

    // initialiseDb();
    initBloc();

    return Scaffold(
        appBar: widget.appBar,
        body: SafeArea(
          child:  ObjectList(),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          backgroundColor: AppColors.royal,
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => ImageCapture(appBar: CourierAppBar(false, true, "Add Item"))));
          },
          heroTag: "demoTag",
          child: Icon(Icons.add),
        )

    );
  }

  void initialiseDb() {
    for(ObjectDto object in ObjectDto.objects) {
      firestoreUtility.addAvailableJob(object);
    }
  }

  var bloc;

  void initBloc() {
    bloc.displayName = ApplicationSingleton.user.name;
    bloc.email = ApplicationSingleton.user.email;
    bloc.setLoggedIn(true);
  }


}

class ObjectList extends StatefulWidget {

  ObjectList();

  @override
  _ObjectListState createState() => _ObjectListState();
}

class _ObjectListState extends State<ObjectList> {

  String searchValue;
  TextEditingController _textController = TextEditingController();

  onItemChanged(String value) {
    setState(() {
      searchValue = value;
    });
  }

  @override
  void initState() {
    // loading = true;
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return  SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: TextField(
                decoration: new InputDecoration(
                  hintText: 'Search item..',
                  labelStyle: TextStyle(color: AppColors.royal, fontSize: 14),
                  fillColor: Colors.blue,
                  focusedBorder:OutlineInputBorder(
                    borderSide: const BorderSide(color: AppColors.royal, width: 2.0),
                    borderRadius:  new BorderRadius.circular(10),
                  ),
                  border: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10),
                    borderSide: new BorderSide(),
                  ),
                  //fillColor: Colors.green
                ),
                controller: _textController,

                onChanged: onItemChanged,),

            ),
            buildJobStream(),
          ],

        )
    );
  }

  Container buildJobStream() {
    GeoPoint geoPoint = new GeoPoint(ApplicationSingleton.position.latitude, ApplicationSingleton.position.longitude);
    Stream<List<DocumentSnapshot>> jobsStream = firestoreUtility.getAvailableJobsNearMe(geoPoint, 1);

    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: StreamBuilder<List<DocumentSnapshot>>(
        stream: jobsStream,
        builder: (BuildContext context,
            AsyncSnapshot<List<DocumentSnapshot>> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return new Center(child: new CircularProgressIndicator());
            default:
              return ListView.builder(
                  shrinkWrap: true,
//                          cacheExtent: 10000.0,
                  physics:NeverScrollableScrollPhysics() ,
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    print("numero "+snapshot.data.length.toString());
                    ObjectDto objectDto = ObjectDto.fromJson(snapshot.data[index].data());
                    if(searchValue==null || searchValue.length==0) {
                      return builObjectTile(objectDto);
                    }
                    if(searchValue!=null && objectDto.name.contains(searchValue)) {
                      return builObjectTile(objectDto);
                    }
                    else {
                      return Container();
                    }
                  }
              );
          }
        },
      ),
    );
  }

  Padding builObjectTile(ObjectDto jobDto) {
    return Padding(
        padding: EdgeInsets.only(top: 20, left: 20, right: 20),
        child: OpenContainer(
            openColor: AppColors.royal,
            closedElevation: 10.0,
            closedShape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            tappable: false,
            transitionType: ContainerTransitionType.fadeThrough,
            transitionDuration: const Duration(milliseconds:800),
            openBuilder: (context, action) {
              return  ObjectView(action, jobDto);
            },
            closedBuilder: (BuildContext c, VoidCallback action) {
              return
                Container(
                  width: 400,
                  height: 200,
                  child: InkWell(
                    onTap: () {
                      action();
                    },
                    child: Objecto.Object(jobDto),
                  ),
                );
            }
        )
    );
  }


}



