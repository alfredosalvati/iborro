import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:iborrow/common/app_colors.dart';
import 'package:iborrow/dto/jobDto.dart';
import 'package:iborrow/widget/objecto.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';


class ObjectView extends StatefulWidget {
  VoidCallback action;
  ObjectDto objectDto;

  ObjectView(this.action, this.objectDto);

  @override
  _ObjectViewState createState() => _ObjectViewState();
}

class _ObjectViewState extends State<ObjectView> with SingleTickerProviderStateMixin{



  @override
  void initState() {

    super.initState();

  }



  @override
  Widget build(BuildContext context) {
    StreamController<List<String>> _controller = StreamController<List<String>>();
    List<String> addresses = [widget.objectDto.name, widget.objectDto.description];
    _controller.add(addresses);



    return Scaffold(

        body: Stack(
          children: [
            buildCard(context),
            buildChevron(),
          ],
        ));
  }

  Positioned buildChevron() {
    return Positioned(
      top: 40,
      left: 20,
      child: new IconButton(
        onPressed: () => widget.action(),
        icon: new Icon(Icons.chevron_left, size: 40), color:AppColors.royal,),
    );
  }


  bool visible = true;
  Widget buildCard(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 50, left: 20, right: 20),
      child:Container(
        // decoration: new BoxDecoration(
        //     boxShadow: [
        //       new BoxShadow(
        //           color: Colors.black.withOpacity(0.3),
        //           offset: new Offset(2.0, 2.0),
        //           blurRadius: 12.0,
        //           spreadRadius: 2.0
        //       )
        //     ],
        //     color: Colors.white, //new Color.fromRGBO(255, 0, 0, 0.0),
        //     borderRadius: new BorderRadius.only(
        //         bottomRight:  Radius.circular(15), //
        //         bottomLeft:  Radius.circular(15), //
        //         topRight: Radius.circular(15))
        //
        // ),
          height: double.infinity,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(image:CachedNetworkImageProvider(widget.objectDto.photo,),
                // height: 130,
                loadingBuilder: (BuildContext context, Widget child,
                    ImageChunkEvent loadingProgress) {
                  if (loadingProgress == null) return child;
                  return Center(
                    child: CircularProgressIndicator(
                      value: loadingProgress.expectedTotalBytes != null
                          ? loadingProgress.cumulativeBytesLoaded /
                          loadingProgress.expectedTotalBytes
                          : null,
                    ),
                  );
                },
              ),
              SizedBox(height: 30,),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                    children: [
                      TextSpan(text:"Item:\n", style:
                      Theme.of(context).textTheme.body1.copyWith(color: AppColors.royal, fontSize: 18,
                          fontWeight: FontWeight.bold)),

                      TextSpan(text:widget.objectDto.name,  style:
                      Theme.of(context).textTheme.body1.copyWith(color: Colors.black, fontSize: 16, height: 1.5)),
                    ]
                ),
              ),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                    children: [
                      TextSpan(text:"Description:\n", style:
                      Theme.of(context).textTheme.body1.copyWith(color: AppColors.royal, fontSize: 18,
                          fontWeight: FontWeight.bold)),

                      TextSpan(text:widget.objectDto.description,  style:
                      Theme.of(context).textTheme.body1.copyWith(color: Colors.black, fontSize: 16, height: 1.5)),
                    ]
                ),
              ),
              SizedBox(height: 30,)
            ],
          )
      ),
    );
  }






}
