import 'dart:async';

import 'package:animations/animations.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:iborrow/bloc/cart_bloc.dart';
import 'package:iborrow/common/app_bar.dart';
import 'package:iborrow/common/app_colors.dart';
import 'package:iborrow/config/application.dart';
import 'package:iborrow/dto/jobDto.dart';
import 'package:iborrow/io/firestore.dart';
import 'package:iborrow/widget/cropper.dart';
import 'package:iborrow/widget/objecto.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import 'object_view.dart';



class MyAvailableObjects extends StatefulWidget {
  final CourierAppBar appBar;

  MyAvailableObjects({Key key, @required this.appBar}) : super(key: key);

  @override
  _MyAvailableObjectsState createState() => _MyAvailableObjectsState();
}

FirestoreUtility firestoreUtility = new FirestoreUtility();

class _MyAvailableObjectsState extends State<MyAvailableObjects> {


  @override
  void initState() {
    // loading = true;
    super.initState();

  }




  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<SessionBloc>(context);

    initBloc();

    return Scaffold(
        appBar: widget.appBar,
        body: SafeArea(
          child:  ObjectList(bloc.email),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          backgroundColor: AppColors.royal,
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => ImageCapture(appBar: CourierAppBar(false, true, "Add Item"))));
          },
          heroTag: "demoTag",
          child: Icon(Icons.add),
        )

    );
  }


  var bloc;

  void initBloc() {
    bloc.displayName = ApplicationSingleton.user.name;
    bloc.email = ApplicationSingleton.user.email;
    bloc.setLoggedIn(true);
  }

}


class ObjectList extends StatelessWidget {

  String email;
  ObjectList(this.email);
  final FirebaseStorage _storage =
  FirebaseStorage(storageBucket: 'gs://iborrow-9bce5.appspot.com');


  @override
  Widget build(BuildContext context) {
    return  SingleChildScrollView(
        child: Column(
          children: [
            buildJobStream(),
          ],

        )
    );
  }

  Container buildJobStream() {
    Stream<QuerySnapshot> jobsStream = firestoreUtility.getAvailableJobs();

    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: StreamBuilder<QuerySnapshot>(
        stream: jobsStream,
        builder: (BuildContext context,
            AsyncSnapshot<QuerySnapshot> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return new Center(child: new CircularProgressIndicator());
            default:

              return ListView.builder(
                  shrinkWrap: true,
//                          cacheExtent: 10000.0,
                  physics:NeverScrollableScrollPhysics() ,
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, index) {

                    ObjectDto objectDto = ObjectDto.fromJson(snapshot.data.docs[index].data());
                    if(email == objectDto.email) {
                      return Dismissible(
                          background: Container(),
                          secondaryBackground: Container(
                            child: Center(
                                child: Text(
                                    'Delete',
                                    style:
                                    Theme.of(context).textTheme.body2.copyWith(color: Colors.white, fontSize: 16))
                            ),
                            color: Colors.red.shade900,
                          ),
                          key: ObjectKey(objectDto.createdDate),
                          child: builObjectTile(objectDto),
                          onDismissed: (direction) {
                            //To delete
                            _storage.getReferenceFromUrl(objectDto.photo).then((value) => value.delete());
                            firestoreUtility.deleteItem(
                                objectDto.createdDate, email);

                          }
                      );
                    } else {
                      return Container();
                    }
                  }
              );
          }
        },
      ),
    );
  }

  Padding builObjectTile(ObjectDto jobDto) {
    return Padding(
        padding: EdgeInsets.only(top: 20, left: 20, right: 20),
        child: OpenContainer(
            openColor: AppColors.royal,
            closedElevation: 10.0,
            closedShape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            tappable: false,
            transitionType: ContainerTransitionType.fadeThrough,
            transitionDuration: const Duration(milliseconds:800),
            openBuilder: (context, action) {
              return  ObjectView(action, jobDto);
            },
            closedBuilder: (BuildContext c, VoidCallback action) {
              return
                Container(
                  width: 400,
                  height: 200,
                  child: InkWell(
                    onTap: () {
                      action();
                    },
                    child: Objecto.Object(jobDto),
                  ),
                );
            }
        )
    );
  }
}
