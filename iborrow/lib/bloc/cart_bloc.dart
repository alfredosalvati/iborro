import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SessionBloc with ChangeNotifier {

  //EMAIL
  String _email;
  String get email => _email;
  set email(String value) {
    _email = value;
  }

  //LOGIN
  bool _loggedIn = false;
  bool get loggedIn => _loggedIn;

  set loggedIn(bool value) {
    _loggedIn = value;
    notifyListeners();
  }

  void setLoggedIn(bool value) {
    _loggedIn = value;
  }


  //LOGIN
  bool _appleLogin = true;
  bool get appleLogin => _appleLogin;

  set appleLogin(bool value) {
    _appleLogin = value;
    notifyListeners();
  }

  void setAppleLoginIn(bool value) {
    _appleLogin = value;
  }

  String get displayName => _name;
  set displayName(String value) {
    _name = value;
  }
  String _name;


  //IS EmailGuest
  bool _isGuest = false;

  bool get isGuest => _isGuest;
  set isGuest(bool value) {
    _isGuest = value;
  } ///// METHODS


  String _state;
  String get state => _state;
  set state(String state) {
    _state = state;
    notifyListeners();

  }


  void logout() {
    _email = null;
    _loggedIn = false;
    notifyListeners();
  }



}