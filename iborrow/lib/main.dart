import 'package:firebase_core/firebase_core.dart';
import 'package:geolocator/geolocator.dart';
import 'package:iborrow/authentication/apple_login.dart';
import 'package:iborrow/common/app_bar.dart';
import 'package:iborrow/config/application.dart';
import 'package:iborrow/dto/jobDto.dart';
import 'package:iborrow/io/firestore.dart';
import 'package:iborrow/pages/item_page.dart';
import 'package:iborrow/pages/login_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:iborrow/widget/cropper.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:provider/provider.dart';

import 'common/app_colors.dart';
import 'bloc/cart_bloc.dart';
import 'common/custom_drawer_guitar.dart';
import 'package:async_loader/async_loader.dart';

import 'common/loading_page.dart';
import 'common/shared_preferences.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

final GlobalKey<AsyncLoaderState> asyncLoaderState =
new GlobalKey<AsyncLoaderState>();

class MyApp extends StatelessWidget {

  Widget _defaultHome;
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    precacheImage(AssetImage("assets/img/background.jpg"), context);

    var _asyncLoader = new AsyncLoader(
      key: asyncLoaderState,
      initState: () async => await init(),
      renderLoad: () => LoadingPage(),
      renderError: ([error]) => CircularProgressIndicator(),
      renderSuccess: ({data}) => _defaultHome,
    );


//GOOGLE MAP CACHE LIMIT
    // Mark as COMPLETE
    // NO BACK BUTTON FOR CURRENT JOB (settings ?)



    return ChangeNotifierProvider<SessionBloc>(
        create: (context) => SessionBloc(),
        child:  MaterialApp(
            debugShowCheckedModeBanner: false,

            theme: ThemeData(
              // This is the theme of your application.
              //
              // Try running your application with "flutter run". You'll see the
              // application has a blue toolbar. Then, without quitting the app, try
              // changing the primarySwatch below to Colors.green and then invoke
              // "hot reload" (press "r" in the console where you ran "flutter run",
              // or simply save your changes to "hot reload" in a Flutter IDE).
              // Notice that the counter didn't reset back to zero; the application
              // is not restarted.
              primarySwatch: Colors.blue,
              splashColor: Colors.grey,
              // This makes the visual density adapt to the platform that you run
              // the app on. For desktop platforms, the controls will be smaller and
              // closer together (more dense) than on mobile platforms.
              visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
            home: Scaffold(
              body:_asyncLoader,
            )
        )
    );
  }

  FirebaseAuth firebaseAuth;

  Future<User> signInAnon() async {
    await Firebase.initializeApp();
    firebaseAuth = FirebaseAuth.instance;
    UserCredential result = await firebaseAuth.signInAnonymously();
    User user = result.user;
    print("Signed in: ${user.uid}");
    return user;
  }

  void init() async {

    signInAnon().then((User user){
      print('Login success!');
      print('UID: ' + user.uid);
    });
    AppleSignInAvailable.check().then((value) => ApplicationSingleton.appleAvailable = value);
    Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high).then((value) =>  ApplicationSingleton.position = value);


    await buildAppleSignIn().whenComplete(() async {
      _defaultHome = LoginPage();

      // Get result of the login function.
      bool _result = await _buildUserProfile();

      if (_result) {
        Widget child = AvailableObjects(appBar: CourierAppBar(false, false, "Available Items"));
        child = CustomGuitarDrawer(child: child);

        _defaultHome = child;

      }
    });







  }

  Future<bool> _buildUserProfile() async {
    List<String> userInfo = await SharedPreferencesHelper.getUserData();
    if(userInfo == null)
      return false;

    ApplicationSingleton.user = new UserDto(userInfo[0], "", userInfo[1], "0888");

    return true;
  }

  FirestoreUtility firestoreUtility = new FirestoreUtility();


  Future<void> buildAppleSignIn() async {
    firestoreUtility.getLoginConfig().then((value) => ApplicationSingleton.appleLogin = value);
  }
}

