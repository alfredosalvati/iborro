import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:iborrow/dto/jobDto.dart';

class FirestoreUtility {
  FirebaseFirestore _db = FirebaseFirestore.instance;

  void addAvailableJob(ObjectDto objectDto) {
    _db.collection("availableItems").doc(objectDto.email+objectDto.createdDate.millisecondsSinceEpoch.toString())
        .set(objectDto.toJson(), SetOptions(merge : true));
  }

  Stream<QuerySnapshot> getAvailableJobs( ) {
    return _db.collection("availableItems").orderBy("createdDate", descending: true).snapshots();
  }


  final geo = Geoflutterfire();
  Stream<List<DocumentSnapshot>> getAvailableJobsNearMe(GeoPoint userGeoPoint, double radius ) {

    GeoFirePoint center = geo.point(latitude: userGeoPoint.latitude, longitude: userGeoPoint.longitude);
    // get the collection reference or query
    var collectionReference = _db.collection("availableItems");

    return geo.collection(collectionRef: collectionReference)
        .within(center: center, radius: radius, field: "position");

  }


  Future<void> deleteItem(Timestamp createDate, String email) {
    print("Deltete "+email+createDate.millisecondsSinceEpoch.toString());
    _db.collection("availableItems")
        .doc(email+createDate.millisecondsSinceEpoch.toString()).delete();
    
  }


  
  Future<QuerySnapshot> getJobsList(String collection ) async {
    return _db.collection(collection).get();
  }

  // Future<void> reserveJob(ObjectDto jobDto, CourierDto courierDto) async {
  //   //GENERATE ORDER ID
  //   final DocumentReference orderRef = _db.collection(ObjectDto.AVAILABLE)
  //       .document(jobDto.id);
  //   _db.runTransaction((Transaction tx) async {
  //
  //     DocumentSnapshot orderSnapshot = await tx.get(orderRef);
  //     if (orderSnapshot.exists) {
  //
  //       ObjectDto job = ObjectDto.fromJson(orderSnapshot.data);
  //       job.courier = courierDto;
  //       job.reservedDate = Timestamp.fromDate(new DateTime.now());
  //
  //       await _db.collection(ObjectDto.RESERVED)
  //           .document(job.id)
  //           .setData( job.toJson(), merge: true);
  //
  //       await tx.delete(orderRef);
  //
  //     }
  //   });
  // }

  // Future<void> startJob(List<ObjectDto> jobs, CourierDto courierDto) async {
  //
  //   jobs.forEach((jobDto) {
  //     final DocumentReference orderRef = _db.collection(ObjectDto.RESERVED)
  //         .document(jobDto.id);
  //     _db.runTransaction((Transaction tx) async {
  //
  //       DocumentSnapshot orderSnapshot = await tx.get(orderRef);
  //       if (orderSnapshot.exists) {
  //
  //         ObjectDto job = ObjectDto.fromJson(orderSnapshot.data);
  //         job.startedDate = Timestamp.fromDate(new DateTime.now());
  //
  //
  //         await _db.collection(ObjectDto.ON_PROGRESS)
  //             .document(job.id)
  //             .setData( job.toJson(), merge: true);
  //
  //         await tx.delete(orderRef);
  //
  //       }
  //     });
  //   });
  // }

//
//   Future<void> finishJob(List<ObjectDto> jobs, CourierDto courierDto) async {
//
//     jobs.forEach((jobDto) {
//       final DocumentReference orderRef = _db.collection(ObjectDto.ON_PROGRESS)
//           .document(jobDto.id);
//       _db.runTransaction((Transaction tx) async {
//
//         DocumentSnapshot orderSnapshot = await tx.get(orderRef);
//         if (orderSnapshot.exists) {
//
//
//           ObjectDto job = ObjectDto.fromJson(orderSnapshot.data);
//           tx.delete(orderRef);
//           print("order delte "+job.orderId);
//
// //          job.courier = null;
// //          _db.collection(JobDto.ON_PROGRESS)
// //              .document(job.id)
// //              .setData( job.toJson(), merge: true);
//
//           job.completedDate = Timestamp.fromDate(new DateTime.now());
//
//           _db.collection(ObjectDto.DONE)
//               .document(job.id)
//               .setData( job.toJson(), merge: true);
//
// //          await tx.delete(orderRef);
//
//         }
//       });
//     });
//   }

  //
  //
  // StreamController<String> _controller =  BehaviorSubject();
  //
  // Stream<String> checkJobs(CourierDto user )  {
  //
  //   getJobs(ObjectDto.ON_PROGRESS).listen((event) {
  //     event.documents.forEach((document) {
  //       ObjectDto jobDto = ObjectDto.fromJson(document.data);
  //       if(jobDto.courier.email == user.email) _controller.add(ObjectDto.ON_PROGRESS);
  //     });
  //   });
  //   getJobs(ObjectDto.RESERVED).listen((event) {
  //     event.documents.forEach((document) {
  //       ObjectDto jobDto = ObjectDto.fromJson(document.data);
  //       if(jobDto.courier.email == user.email) _controller.add(ObjectDto.RESERVED);
  //     });
  //   });
  //
  //    _controller.add(ObjectDto.AVAILABLE);
  //
  //    return _controller.stream.asBroadcastStream();
  // }
  //
  // Future<String> getReservedJobs( String email ) async {
  //   String type = "";
  //
  //   QuerySnapshot snapshot = await _db.collection(ObjectDto.RESERVED).getDocuments();
  //
  //    snapshot.documents.forEach((document) {
  //      ObjectDto jobDto = ObjectDto.fromJson(document.data);
  //      if(jobDto.courier.email == email) type = ObjectDto.RESERVED;
  //    });
  //
  //    return type;
  //
  // }
  //
  //
  // Future<String> getOnProgressJobs( String email ) async {
  //   String type = "";
  //   QuerySnapshot snapshot = await _db.collection(ObjectDto.ON_PROGRESS).getDocuments();
  //   snapshot.documents.forEach((document) {
  //     ObjectDto jobDto = ObjectDto.fromJson(document.data);
  //     if(jobDto.courier.email == email) {
  //       type = ObjectDto.ON_PROGRESS;
  //     }
  //   });
  //
  //   return type;
  // }
  //
  // Future<void> sendLocationData(String email, LocationData currentLocation) {
  //   print("SEND");
  //   return _db.collection("couriers").document(email).collection("personalDetails").document(email)
  //       .setData({"lastPosition" : toJson(currentLocation)}, merge: true);
  // }
  //
  // Map<String, dynamic> toJson(LocationData locationData) {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['latitude'] = locationData.latitude;
  //   data['longitude'] = locationData.longitude;
  //   data['altitude'] = locationData.altitude;
  //   data['accuracy'] = locationData.accuracy;
  //   data['speed'] = locationData.speed;
  //   data['speedAccuracy'] = locationData.speedAccuracy;
  //   data['heading'] = locationData.heading;
  //   data['time'] = locationData.time;
  //
  //   return data;
  // }



  addUpdateUser(UserDto userDto) {
    _db.collection("users").doc(userDto.email).collection("personalDetails").doc(userDto.email)
        .set({"email":userDto.email, "providerId":"google",
      "displayName":userDto.name, }, SetOptions(merge : true));
  }

  Future<UserDto> getUser(String email)  async {
    DocumentSnapshot documentSnapshot = await _db.collection("users")
        .doc(email).collection("personalDetails").doc(email).get();

    if(documentSnapshot.exists) {
      return UserDto.fromJson(documentSnapshot.data());
    }
    return null;
  }

  Future<bool> getLoginConfig() async {
    DocumentSnapshot documentSnapshot = await _db.collection("config")
        .doc("apple").get();

    if(documentSnapshot.exists) {
      return documentSnapshot.get("enabled");
    }
    return true;
  }





}