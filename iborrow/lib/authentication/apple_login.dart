
import 'package:apple_sign_in/apple_id_request.dart';
import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:apple_sign_in/scope.dart';
import 'package:iborrow/bloc/cart_bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:iborrow/common/app_bar.dart';
import 'package:iborrow/common/custom_drawer_guitar.dart';
import 'package:iborrow/config/application.dart';
import 'package:iborrow/dto/jobDto.dart';
import 'package:iborrow/pages/item_page.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';


class AppleLogin extends StatefulWidget {
  @override
  _AppleLoginState createState() => _AppleLoginState();
}

class _AppleLoginState extends State<AppleLogin> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  User user;
  SharedPreferences sharedPreferences;

  var email;

  var bloc;

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((value) => sharedPreferences = value);
  }

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<SessionBloc>(context);

    return
      SignInButton(
        Buttons.AppleDark,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        onPressed: () async {
          await signInWithApple().then((value) {
            Widget child = AvailableObjects(
                appBar: CourierAppBar(false, false, "Available Items"));
            child = CustomGuitarDrawer(child: child);

            Navigator.of(context).pushReplacement(
              MaterialPageRoute<void>(builder: (_) => Scaffold(body: child)),
            );
          });
        },
      );
  }

  Future<void> signInWithApple(
      {List<Scope> scopes = const [Scope.email, Scope.fullName]}) async {
    // 1. perform the sign-in request
    final result = await AppleSignIn.performRequests(
        [AppleIdRequest(requestedScopes: scopes)]);
    // 2. check the result
    switch (result.status) {
      case AuthorizationStatus.authorized:
        final appleIdCredential = result.credential;
        final oAuthProvider = OAuthProvider('apple.com');
        final credential = oAuthProvider.credential(
          idToken: String.fromCharCodes(appleIdCredential.identityToken),
          accessToken:
          String.fromCharCodes(appleIdCredential.authorizationCode),
        );
        final authResult = await _auth.signInWithCredential(credential);
        email = authResult.user.email;

        if (email == null) {
          throw PlatformException(
            code: 'NO_EMAIL',
            message: 'Sign in aborted by user',
          );
        }

        //CHECK IF IT EXISTS OR ADD IT
        UserDto userDto = await firestoreUtility.getUser(email);
        if (userDto == null) {
          userDto = new UserDto("Anonymous", "", email, "0888");
          userDto.login = Timestamp.fromDate(new DateTime.now());
          firestoreUtility.addUpdateUser(userDto);
        }

        ApplicationSingleton.user = userDto;
        List<String> userInfo = ["Anonymous", email];
        sharedPreferences.setStringList("user", userInfo);


        bloc.loggedIn = true;
        bloc.email = email;
        bloc.displayName = "Anonymous";
    }
  }
}



class AppleSignInAvailable {

  static Future<bool> check() async {
    return await AppleSignIn.isAvailable();
  }
}


