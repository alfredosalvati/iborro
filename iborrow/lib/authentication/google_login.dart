import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:iborrow/bloc/cart_bloc.dart';
import 'package:iborrow/common/app_bar.dart';
import 'package:iborrow/common/custom_drawer_guitar.dart';
import 'package:iborrow/config/application.dart';
import 'package:iborrow/dto/jobDto.dart';
import 'package:iborrow/io/firestore.dart';
import 'package:iborrow/pages/item_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';



class GoogleLogin extends StatefulWidget {
  @override
  _GoogleLoginState createState() => _GoogleLoginState();
}

class _GoogleLoginState extends State<GoogleLogin> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  User user;
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  SharedPreferences sharedPreferences;
  FirestoreUtility firestoreUtility = new FirestoreUtility();


  var bloc;

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((value) => sharedPreferences = value);

  }

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<SessionBloc>(context);

    return
      SignInButton(
        Buttons.GoogleDark,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        onPressed: () async {
          await signInWithGoogle().then((value) {

            Widget child = AvailableObjects(appBar: CourierAppBar(false, false, "Available Items"));
            child = CustomGuitarDrawer(child: child);

            Navigator.of(context).pushReplacement(
              MaterialPageRoute<void>(builder: (_) => Scaffold(body:child)),
            );
          });
        },
      );
  }

  Future<void> signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount =
    await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
    await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final UserCredential authResult = await _auth.signInWithCredential(credential);
    user = authResult.user;

    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final User currentUser = await _auth.currentUser;
    assert(user.uid == currentUser.uid);


    //CHECK IF IT EXISTS OR ADD IT
    UserDto userDto = await firestoreUtility.getUser(user.email);
    if(userDto == null) {
      userDto = new UserDto(user.displayName, "", user.email, "0888");
      userDto.login = Timestamp.fromDate(new DateTime.now());
      firestoreUtility.addUpdateUser(userDto);
    }

    ApplicationSingleton.user = userDto;
    List<String> userInfo = [user.displayName, user.email];
    sharedPreferences.setStringList("user", userInfo);


    bloc.loggedIn =true;
    bloc.email = user.email;
    bloc.displayName = user.displayName;
  }


}
