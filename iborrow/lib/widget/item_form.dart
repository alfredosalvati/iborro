
import 'package:flutter/material.dart';
import 'package:iborrow/common/app_colors.dart';

class ItemForm extends StatefulWidget {
  TextEditingController textNameController;
  TextEditingController textDescriptionController;


  ItemForm(this.textNameController, this.textDescriptionController);

  @override
  _ItemFormState createState() => _ItemFormState();

}


class _ItemFormState extends State<ItemForm> {

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  ThemeData theme;



  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    theme = Theme.of(context);
    return Form(
        key: _formKey,
        autovalidateMode: AutovalidateMode.always,
        child: Container(
            height: 200,
            child: ListView(
              children: [
                Padding(padding: EdgeInsets.only(left: 20, right: 20, top: 15),
                  child: TextFormField(
                      controller: widget.textNameController,
                      decoration: new InputDecoration(
                        labelText: "Item:",
                        labelStyle: TextStyle(color: AppColors.royal, fontSize: 16),
                        fillColor: Colors.transparent,
                        focusedBorder:OutlineInputBorder(
                          borderSide: const BorderSide(color: AppColors.royal, width: 2.0),
                          borderRadius:  new BorderRadius.circular(10),
                        ),
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10),
                          borderSide: new BorderSide(),
                        ),
                        //fillColor: Colors.green
                      ),
                      autofocus: false,
                      obscureText: false,
                      cursorColor: AppColors.royal,
                      validator:  (val) => val.isEmpty ? 'Name is required' : null,
                      style:
                      theme.textTheme.body1.copyWith(color: Colors.black, fontSize: 16)
                  ),
                ),
                Padding(padding: EdgeInsets.only(left: 20, right: 20, top: 15),
                    child: TextFormField(
                        controller: widget.textDescriptionController,
                        decoration: new InputDecoration(
                          labelText: "Description:",
                          labelStyle: TextStyle(color: AppColors.royal, fontSize: 16),
                          fillColor: Colors.transparent,
                          focusedBorder:OutlineInputBorder(
                            borderSide: const BorderSide(color: AppColors.royal, width: 2.0),
                            borderRadius:  new BorderRadius.circular(10),
                          ),
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10),
                            borderSide: new BorderSide(),
                          ),
                          //fillColor: Colors.green
                        ),
                        autofocus: false,
                        obscureText: false,
                        cursorColor: AppColors.royal,
                        style:
                        theme.textTheme.body1.copyWith(color: Colors.black, fontSize: 16)
                    )
                )
              ],
            )
        )
    );
  }
}