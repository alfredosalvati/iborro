import 'dart:io';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:iborrow/common/app_bar.dart';
import 'package:iborrow/common/app_colors.dart';
import 'package:iborrow/widget/uploader.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:location_permissions/location_permissions.dart';

import 'item_form.dart';

class ImageCapture extends StatefulWidget {
  final CourierAppBar appBar;

  ImageCapture({Key key, @required this.appBar}) : super(key: key);

  createState() => _ImageCaptureState();
}

class _ImageCaptureState extends State<ImageCapture> {
  /// Active image file
  File _imageFile;


  /// Remove image
  void _clear() {
    setState(() => _imageFile = null);
  }

  TextEditingController textNameController = new TextEditingController();
  TextEditingController textDescriptionController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Hero(
      child: Scaffold(
        appBar: widget.appBar,
        // Select an image from the camera or gallery

        // Preview the image and crop it
        body: ListView(
          children: <Widget>[
            ItemForm(textNameController, textDescriptionController),
            if (_imageFile != null) ...[

              Image.file(File(_imageFile.path)),

              Row(
                children: <Widget>[
                  FlatButton(
                    child: Icon(Icons.crop, color: AppColors.royal,),
                    onPressed: _cropImage,
                  ),
                  FlatButton(
                    child: Icon(Icons.refresh, color: AppColors.royal,),
                    onPressed: _clear,
                  ),
                ],
              ),

              Uploader(File(_imageFile.path), textNameController.text, textDescriptionController.text),
              SizedBox(height: 30,)
            ]
            else ...[
              Padding(padding: EdgeInsets.only(top: 20)),
              Center(
                child: Text("Add photo", style: Theme.of(context).textTheme.body1.copyWith(color: AppColors.royal,
                    fontWeight: FontWeight.bold, fontSize: 18)
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 20)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  InkWell(
                    onTap: () =>_pickImage(ImageSource.camera),
                    child: Icon(Icons.photo_camera, color: AppColors.royal, size: 130),
                  ),
                  InkWell(
                    child: Icon(Icons.photo_library, color: AppColors.royal, size: 130,),
                    onTap: () => _pickImage(ImageSource.gallery),
                  )
                ],
              )
            ]



          ],
        ),
      ),
      tag: "demoTag",
    );
  }


  /// Cropper plugin
  Future<void> _cropImage() async {
    File cropped = await ImageCropper.cropImage(
        sourcePath: _imageFile.path,
        aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1),
        compressQuality:70
    );

    setState(() {
      _imageFile = cropped ?? _imageFile;
    });
  }

  /// Select an image via gallery or camera
  Future<void> _pickImage(ImageSource source) async {
    ImagePicker imagePicker = ImagePicker();
    PickedFile selected = await imagePicker.getImage(source: source, imageQuality: 80);

    setState(() {
      _imageFile = File(selected.path);
    });
  }

  LocationPermission permission;

  @override
  void initState() {
    super.initState();
    Geolocator.requestPermission().then((onValue) {
      if(onValue == LocationPermission.denied || onValue == LocationPermission.deniedForever) {
        //alert
      }
    });
  }

}
