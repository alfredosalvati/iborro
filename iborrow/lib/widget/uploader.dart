import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:geolocator/geolocator.dart';
import 'package:iborrow/bloc/cart_bloc.dart';
import 'package:iborrow/common/app_colors.dart';
import 'package:iborrow/dto/jobDto.dart';
import 'package:iborrow/io/firestore.dart';
import 'package:provider/provider.dart';

class Uploader extends StatefulWidget {

  File file;
  String name;
  String description;

  Uploader(this.file, this.name, this.description);

  @override
  _UploaderState createState() => _UploaderState();
}


class _UploaderState extends State<Uploader> {
  final FirebaseStorage _storage =
  FirebaseStorage(storageBucket: 'gs://iborrow-9bce5.appspot.com');

  StorageUploadTask _uploadTask;
  String url;
  FirestoreUtility firestoreUtility = new FirestoreUtility();
  final geo = Geoflutterfire();


  Position position;
  @override
  void initState() {
    super.initState();
    Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high).then((value) => position = value);
  }


  /// Starts an upload task
  void _startUpload() {

    /// Unique file name for the file
    String filePath = '${DateTime.now()}.png';

    setState(() {
      _uploadTask = _storage.ref().child(filePath).putFile(widget.file);
    });
  }
  var bloc;

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<SessionBloc>(context);

    if (_uploadTask != null) {

      /// Manage the task state and event subscription with a StreamBuilder
      return StreamBuilder<StorageTaskEvent>(
          stream: _uploadTask.events,
          builder: (_, snapshot) {
            var event = snapshot?.data?.snapshot;

            double progressPercent = event != null
                ? event.bytesTransferred / event.totalByteCount
                : 0;

            if(_uploadTask.isComplete) {
              event.ref.getDownloadURL().then((url) {
                GeoFirePoint myLocation = geo.point(latitude: position.latitude, longitude: position.longitude);
                ObjectDto objectDto = ObjectDto(widget.name, widget.description, url, bloc.email, Timestamp.now(), myLocation.data);
                firestoreUtility.addAvailableJob(objectDto);

                Navigator.of(context).pop();

              });

            }

            return Column(
              children: [
                if (_uploadTask.isComplete)
                  Text('🎉🎉🎉'),


                // if (_uploadTask.isPaused)
                //   FlatButton(
                //     child: Icon(Icons.play_arrow),
                //     onPressed: _uploadTask.resume,
                //   ),
                //
                // if (_uploadTask.isInProgress)
                //   FlatButton(
                //     child: Icon(Icons.pause),
                //     onPressed: _uploadTask.pause,
                //   ),

                // Progress bar
                LinearProgressIndicator(value: progressPercent),
                Text(
                    '${(progressPercent * 100).toStringAsFixed(2)} % '
                ),
              ],
            );
          });


    } else {
      // Allows user to decide when to start the upload
      return FlatButton.icon(
        label: Text('Upload', style: Theme.of(context).textTheme.body1.copyWith(color: AppColors.royal,
            fontWeight: FontWeight.bold, fontSize: 16)),
        icon: Icon(Icons.cloud_upload, color: AppColors.royal,),
        onPressed: _startUpload,
      );

    }
  }
}