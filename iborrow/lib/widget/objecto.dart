import 'package:cached_network_image/cached_network_image.dart';
import 'package:iborrow/common/app_colors.dart';
import 'package:iborrow/bloc/cart_bloc.dart';
import 'package:iborrow/dto/jobDto.dart';
import 'package:iborrow/io/firestore.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:mailto/mailto.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class Objecto extends StatelessWidget {

  ObjectDto objectDto;
  FirestoreUtility firestoreUtility = new FirestoreUtility();
  var bloc;

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<SessionBloc>(context);

    return Container(
//      color: Colors.red,
        height: 100,
        padding: EdgeInsets.only(left:10 , right: 10),
        child: Column(
          children: [
            SizedBox(height: 15,),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(flex: 2,
                    child: Image(image:CachedNetworkImageProvider(objectDto.photo,),
                      height: 130,
                      loadingBuilder: (BuildContext context, Widget child,
                          ImageChunkEvent loadingProgress) {
                        if (loadingProgress == null) return child;
                        return Center(
                          child: CircularProgressIndicator(
                            value: loadingProgress.expectedTotalBytes != null
                                ? loadingProgress.cumulativeBytesLoaded /
                                loadingProgress.expectedTotalBytes
                                : null,
                          ),
                        );
                      },
                    )),

                Expanded(flex: 1,
                    child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            buildName(context,),
                            buildDescription(context),
                          ],
                        )
                    )
                ),
              ],
            ),
            SizedBox(height: 10,),
            bloc.email != objectDto.email ? buildChoseJob(context) : Container()

          ],
        )
//      )
    );
  }

  Container buildChoseJob(BuildContext context) {
    return Container(
        height: 30,
        width: 100,
        decoration: new BoxDecoration(
          borderRadius: new BorderRadius.all(Radius.circular(10)),
        ),
        child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child:  Text("Email",  textAlign: TextAlign.center,style:
            Theme.of(context).textTheme.body1.copyWith(color: Colors.white, fontSize: 16)),
            color: AppColors.royal,
            onPressed: () async {

              final mailtoLink = Mailto(
                to: [objectDto.email],
                subject: objectDto.name,
                body: 'Hello,\nWould it be possible to borrow the '+objectDto.name,
              );
              // Convert the Mailto instance into a string.
              // Use either Dart's string interpolation
              // or the toString() method.
              await launch('$mailtoLink');
            }

        ));
  }

  Container buildName(BuildContext context) {
    return Container(
      // height: 70,
        width: double.infinity,
        // decoration: new BoxDecoration(
        //   color: AppColors.grey, //new Color.fromRGBO(255, 0, 0, 0.0),
        //   borderRadius: new BorderRadius.all(Radius.circular(10)),
        // ),
        child: Container(
            padding: EdgeInsets.all(5),
            child: SingleChildScrollView(
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      children: [
                        TextSpan(text:"Item:\n", style:
                        Theme.of(context).textTheme.body1.copyWith(color: AppColors.royal, fontSize: 14,
                            fontWeight: FontWeight.bold)),

                        TextSpan(text:objectDto.name,  style:
                        Theme.of(context).textTheme.body1.copyWith(color: Colors.black, fontSize: 14, height: 1.5)),
                      ]
                  ),
                )
            )
        ));
  }
  String formatChosenDate(DateTime dateTime) {
    return formatDate(dateTime, [dd, '/', mm, '/', yyyy ,' ', HH, ':', nn]);
  }
  Container buildDescription(BuildContext context) {
    return Container(
        width: double.infinity,
        // height: 70,
        // decoration: new BoxDecoration(
        //   color: AppColors.grey, //new Color.fromRGBO(255, 0, 0, 0.0),
        //   borderRadius: new BorderRadius.all(Radius.circular(10)),
        // ),
        child: Container(
            padding: EdgeInsets.all(5),
            child: SingleChildScrollView(
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      children: [
                        //             TextSpan(text: formatChosenDate(objectDto.createdDate.toDate())+"\n", style:
                        // Theme.of(context).textTheme.body1.copyWith(color: AppColors.royal, fontSize: 12)),
                        TextSpan(text:"Description:\n", style:
                        Theme.of(context).textTheme.body1.copyWith(color: AppColors.royal, fontSize: 14,
                            fontWeight: FontWeight.bold)),
                        TextSpan(text:objectDto.description, style:
                        Theme.of(context).textTheme.body1.copyWith(color: Colors.black, fontSize: 14, height: 1.5)),
                      ]
                  ),
                )
            )
        )

    );
  }

  Objecto.Object(this.objectDto);

  onError(Object error) {
    print(error);
  }

  Objecto(this.objectDto);


}
