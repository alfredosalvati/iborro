

import 'package:geolocator/geolocator.dart';
import 'package:iborrow/dto/jobDto.dart';

class ApplicationSingleton {

  static UserDto user;
  static bool appleAvailable;
  static bool appleLogin = true;
  static Position position;

}
