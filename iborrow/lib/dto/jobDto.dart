
import 'package:cloud_firestore/cloud_firestore.dart';

class UserDto {
  String name;
  String surname;
  String email;
  String phone;
  Timestamp login;


  UserDto(this.name, this.surname, this.email, this.phone);

  UserDto.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    surname = json['surname'];
    email = json['email'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['surname'] = this.surname;
    data['email'] = this.email;
    data['phone'] = this.phone;

    return data;
  }
}

// class CourierDto extends UserDto {
//
//   static final String BIKE = "bike";
//   static final String MOTOR_BIKE = "motorbike";
//
//   String vehicle;
//
//   CourierDto(String name, String surname, String email, String phone, this.vehicle) : super(name, surname, email, phone);
//
//   @override
//   CourierDto.fromJson(Map<String, dynamic> json) : super.fromJson(json) {
//     vehicle = json['vehicle'];
//    
//   }
//
//   Map<String, dynamic> toJson() {
//     return super.toJson()..putIfAbsent("vehicle", () => this.vehicle);
//   }
//
// }


class ObjectDto {
  String name;
  String description;
  String photo;

  String email;
  Timestamp createdDate;
  dynamic position;



//  String status;

  // static final String RESERVED = "reservedJobs";
  // static final String ON_PROGRESS = "onprogressJobs";
  // static final String AVAILABLE = "availableJobs";
  // static final String DONE = "doneJobs";
  // static final String CANCELLED = "cancelledJobs";




  ObjectDto(this.name, this.description, this.photo, this.email, this.createdDate, this.position);

  ObjectDto.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    description = json['description'];
    photo = json['photo'];
    email = json['email'];

    if(json['createdDate']!=null ){
      createdDate = json['createdDate'];
    }
    if(json['position']!=null) {
      position = json['position'];
    }

//    status = json['status'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['description'] = this.description;
    data['email'] = this.email;
    data['photo'] = this.photo;
    if(this.createdDate != null)
      data['createdDate'] = this.createdDate;
    if(this.position != null)
      data['position'] = this.position;

    return data;
  }

  // static final UserDto userSender =  UserDto( "GoLuxe", "", "xander@keytofood.co.uk", "07494168872");
  // static final UserDto useerRecipient1 =  UserDto( "TOM", "WINCHESTER", "", "07763XXXXXXX");
  // static final UserDto useerRecipient2 =  UserDto( "benjamin", "faulkner", "", "07779035258");
  // static final UserDto useerRecipient3 =  UserDto( "Lisa ", "Barash", "", "07986500282");
  // static final UserDto useerRecipient4 =  UserDto( "SABRINA", "WISELY", "", "07823330546");
  // static final UserDto useerRecipient5 =  UserDto( "BEN", "JAY", "", "07731665401");
  // static final UserDto useerRecipient6 =  UserDto( "RORY ", "BROWN", "", "07889141950");
  // static final UserDto useerRecipient7 =  UserDto( "DAVIDE 	", "RONCHI", "", "");
  // static final UserDto useerRecipient8 =  UserDto( "Sheena ", "Shah", "", "07967228857");
  // static final UserDto useerRecipient9 =  UserDto( "RAMY 	", "FANOUS", "", "07956397548");
  // static final UserDto useerRecipient10 =  UserDto( "KERI", "O RIORDAN", "", "07837816762");
  // static final UserDto useerRecipient11 =  UserDto( "CORRINA 	", "CALORI", "", "07578941448");
  // static final UserDto useerRecipient12 =  UserDto( "Ziyi ", "Xue", "da", "07394302350");
  // static final UserDto useerRecipient13 =  UserDto( "Anna", "Diski", "", "07956756677");
  //
  static List<ObjectDto> objects = [
    // ObjectDto( "Drill", "open holes", "https://firebasestorage.googleapis.com/v0/b/iborrow-9bce5.appspot.com/o/drill.jpg?alt=media&token=9b08b481-e237-444c-978d-ae98800073a5", "alfsalvati@gmail.com", Timestamp.now()),
    // ObjectDto( "Drill", "open holes", "https://firebasestorage.googleapis.com/v0/b/iborrow-9bce5.appspot.com/o/drill.jpg?alt=media&token=9b08b481-e237-444c-978d-ae98800073a5", "alfsalvati@gmail.com", Timestamp.now()),
    // ObjectDto( "Drill", "open holes", "https://firebasestorage.googleapis.com/v0/b/iborrow-9bce5.appspot.com/o/drill.jpg?alt=media&token=9b08b481-e237-444c-978d-ae98800073a5", "alfsalvati@gmail.com", Timestamp.now())

  ];
}


