import 'dart:math' as math;

import 'package:iborrow/common/app_colors.dart';
import 'package:iborrow/authentication/google_login.dart';
import 'package:iborrow/bloc/cart_bloc.dart';
import 'package:iborrow/config/application.dart';
import 'package:iborrow/pages/login_page.dart';
import 'package:flutter/material.dart';
import 'package:iborrow/pages/my_item_page.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app_bar.dart';

class CustomGuitarDrawer extends StatefulWidget {
  final Widget child;

  const CustomGuitarDrawer({Key key, this.child}) : super(key: key);

  static CustomGuitarDrawerState of(BuildContext context) =>
      context.findAncestorStateOfType<CustomGuitarDrawerState>();

  @override
  CustomGuitarDrawerState createState() => new CustomGuitarDrawerState();
}

class CustomGuitarDrawerState extends State<CustomGuitarDrawer>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  bool _canBeDragged = false;
  final double maxSlide = 300.0;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 250),
    );
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  void toggle() => animationController.isDismissed
      ? animationController.forward()
      : animationController.reverse();

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onHorizontalDragStart: _onDragStart,
      onHorizontalDragUpdate: _onDragUpdate,
      onHorizontalDragEnd: _onDragEnd,
      behavior: HitTestBehavior.translucent,
      //onTap: toggle,
      child: AnimatedBuilder(
        animation: animationController,
        builder: (context, _) {
          return Material(
            color: AppColors.grey,
            child: Stack(
              children: <Widget>[
                Transform.translate(
                  offset: Offset(maxSlide * (animationController.value - 1), 0),
                  child: Transform(
                    transform: Matrix4.identity()
                      ..setEntry(3, 2, 0.001)
                      ..rotateY(math.pi / 2 * (1 - animationController.value)),
                    alignment: Alignment.centerRight,
                    child: MyDrawer(),
                  ),
                ),
                Transform.translate(
                  offset: Offset(maxSlide * animationController.value, 0),
                  child: Transform(
                    transform: Matrix4.identity()
                      ..setEntry(3, 2, 0.001)
                      ..rotateY(-math.pi * animationController.value / 2),
                    alignment: Alignment.centerLeft,
                    child: widget.child,
                  ),
                ),
                Positioned(
                  top: 4.0 + MediaQuery.of(context).padding.top,
                  left: 4.0 + animationController.value * maxSlide,
                  child: IconButton(
                    icon: Icon(Icons.menu),
                    onPressed: toggle,
                    color: AppColors.grey,
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  void _onDragStart(DragStartDetails details) {
    bool isDragOpenFromLeft = animationController.isDismissed;
    bool isDragCloseFromRight = animationController.isCompleted;
    _canBeDragged = isDragOpenFromLeft || isDragCloseFromRight;
  }

  void _onDragUpdate(DragUpdateDetails details) {
    if (_canBeDragged) {
      double delta = details.primaryDelta / maxSlide;
      animationController.value += delta;
    }
  }

  void _onDragEnd(DragEndDetails details) {
    //I have no idea what it means, copied from Drawer
    double _kMinFlingVelocity = 365.0;

    if (animationController.isDismissed || animationController.isCompleted) {
      return;
    }
    if (details.velocity.pixelsPerSecond.dx.abs() >= _kMinFlingVelocity) {
      double visualVelocity = details.velocity.pixelsPerSecond.dx /
          MediaQuery.of(context).size.width;

      animationController.fling(velocity: visualVelocity);
    } else if (animationController.value < 0.5) {
      animationController.reverse();
    } else {
      animationController.forward();
    }
  }
}

class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {

  SessionBloc bloc;

  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((value) => sharedPreferences = value);

  }

  void logout() async  {
    bloc.loggedIn = false;
    bloc.logout();
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.clear();
//    setState(() {
//    });
    print("User Sign Out");
  }

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<SessionBloc>(context);

    return SizedBox(
      width: 300,
      height: double.infinity,
      child: Material(
        color: AppColors.royal,
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [

              sharedPreferences != null && sharedPreferences.getStringList("user") != null &&
            sharedPreferences.getStringList("user").isNotEmpty  ?
              Container(
                height: 70,
                padding: EdgeInsets.only(left:15, top: 12),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text("Hi, "+sharedPreferences.getStringList("user").first, style:
                    Theme.of(context).textTheme.body1.copyWith(color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),
                    )),
              ) : Container(
                height: 50,
              ),
//                Image.asset(
//                  'assets/flutter_europe_white.png',
//                  width: 200,
//                ),
//                ListTile(
//                  leading: Icon(Icons.new_releases),
//                  title: Text('News'),
//                ),
              ListTile(
                  leading: Icon(Icons.wysiwyg_sharp, color: Colors.white,),
                  title: Text('My Items', style:
                  Theme.of(context).textTheme.body1.copyWith(color: Colors.white)),
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute<void>(builder: (_) => MyAvailableObjects(appBar: CourierAppBar(false, true, "My Items"))),
                    );
                  }
              ),
              // ListTile(
              //     leading: Icon(Icons.settings),
              //     title: Text('Settings'),
              //     onTap: null,
              // ),
              bloc.loggedIn ? ListTile(
                  leading: Icon(Icons.exit_to_app, color: Colors.white,),
                  title: Text('Logout', style:
                  Theme.of(context).textTheme.body1.copyWith(color: Colors.white)),

                  onTap: () {
                    logout();
                    Navigator.of(context).push(
                      MaterialPageRoute<void>(builder: (_) => LoginPage()),
                    );
                  }
              ): Container()
            ],
          ),
        ),
      ),
    );


  }

}




