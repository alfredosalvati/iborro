import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  ///
  /// Instantiation of the SharedPreferences library
  ///
  final String _kNotificationsPrefs = "allowNotifications";
  final String _kSortingOrderPrefs = "sortOrder";

  static final String _user = "user";



  /// ------------------------------------------------------------
  /// Method that returns the user decision to allow notifications
  /// ------------------------------------------------------------
  static Future<List<String>> getUserData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getStringList(_user) ?? null;
  }


//
//  /// ------------------------------------------------------------
//  /// Method that returns the user decision to allow notifications
//  /// ------------------------------------------------------------
//  Future<bool> getAllowsNotifications() async {
//    final SharedPreferences prefs = await SharedPreferences.getInstance();
//
//    return prefs.getBool(_kNotificationsPrefs) ?? false;
//  }
//
//  /// ----------------------------------------------------------
//  /// Method that saves the user decision to allow notifications
//  /// ----------------------------------------------------------
//  Future<bool> setAllowsNotifications(bool value) async {
//    final SharedPreferences prefs = await SharedPreferences.getInstance();
//
//    return prefs.setBool(_kNotificationsPrefs, value);
//  }
//
//  /// ------------------------------------------------------------
//  /// Method that returns the user decision on sorting order
//  /// ------------------------------------------------------------
//  Future<String> getSortingOrder() async {
//    final SharedPreferences prefs = await SharedPreferences.getInstance();
//
//    return prefs.getString(_kSortingOrderPrefs) ?? 'name';
//  }
//
//  /// ----------------------------------------------------------
//  /// Method that saves the user decision on sorting order
//  /// ----------------------------------------------------------
//  Future<bool> setSortingOrder(String value) async {
//    final SharedPreferences prefs = await SharedPreferences.getInstance();
//
//    return prefs.setString(_kSortingOrderPrefs, value);
//  }
}