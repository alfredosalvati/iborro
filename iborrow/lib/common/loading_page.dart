import 'package:iborrow/common/app_colors.dart';
import 'package:flutter/material.dart';

class LoadingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      alignment: Alignment.center,
      color: AppColors.royal,
      child: Image.asset("assets/img/cycleGif.gif", width: 200),
      padding: const EdgeInsets.all(20.0),
    );
  }
}
