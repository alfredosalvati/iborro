
import 'package:flutter/material.dart';

import 'app_colors.dart';


class Dialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key,) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  key: key,
                  backgroundColor: AppColors.grey,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(bottom: 30),
                      child: Align(
                          alignment: Alignment.center,
                          child: Text("Start delivering!", style: Theme.of(context).textTheme.display1.copyWith(color: Colors.black))
                      ),
                    ),
                   RaisedButton(
                     onPressed: () => Navigator.of(context).pop(),
                     child: Text("Ok", style: Theme.of(context).textTheme.display1.copyWith(color: Colors.white)),
                     color: AppColors.royal,
                   )
                  ]));
        });
  }


}
