
import 'package:iborrow/bloc/cart_bloc.dart';
import 'package:iborrow/config/application.dart';
import 'package:iborrow/dto/jobDto.dart';
import 'package:iborrow/io/firestore.dart';
import 'package:iborrow/pages/item_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'app_colors.dart';
import 'custom_drawer_guitar.dart';

class CourierAppBar extends StatelessWidget implements PreferredSizeWidget {
  bool action;
  bool leading;
  String title;

  CourierAppBar(this.action,  this.leading, this.title,);

  /// you can add more fields that meet your needs


  @override
  Size get preferredSize => new Size.fromHeight(56);

  var bloc;

  @override
  AppBar build(BuildContext context) {
    bloc = Provider.of<SessionBloc>(context);

    return new AppBar(
      flexibleSpace: Container(
        decoration: BoxDecoration(
          color: AppColors.royal,
        ),
      ),
      actions: <Widget>[
        action ?  Padding(
          padding: EdgeInsets.only(right: 10),
          child: IconButton(icon:  Icon(Icons.directions_bike,),
            color: AppColors.grey,
            disabledColor: Colors.grey,
            onPressed: null,
          ),
        ) : Container()
      ],
      title: Text(title, textAlign: TextAlign.center, style:
      Theme.of(context).textTheme.body1.copyWith(color: AppColors.grey, fontSize: 22, fontWeight: FontWeight.bold)),
      backgroundColor: AppColors.background,
      leading: leading?  IconButton(
        icon: Icon(Icons.chevron_left, color: AppColors.grey,),
        onPressed: () {
          Widget child = AvailableObjects(appBar: CourierAppBar(false, false, "Available Items"));
          child = CustomGuitarDrawer(child: child);
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => child));
        }
      ) : Container(),
      elevation: 0,
    );
  }


}