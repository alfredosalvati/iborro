import 'package:flutter/material.dart';

class AppColors {
//  static const Color yellow = const Color(0xfffdc200);
  static const Color royal = const Color(0xff4c5e9a);
  static const Color red = const Color(0xffCD304F);
  static const Color grey = const Color(0xffF7F7F7);
  static const Color blue = const Color(0xff1c819e);
  static const Color background = const Color(0xfffafafa);
}